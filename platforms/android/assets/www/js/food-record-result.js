
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() { 
        if(localStorage.length==0) {
            $('.layer-overlay2').addClass('active');
        
            setTimeout(function()
            {
                window.location.href = "record_food.html";
            }, 400);
        }
    },
    errorPop: function(tx, err) {
        alert("Error populating DB: "+err);
    },
    errorCB: function(tx, err) {
        alert("Error processing SQL: "+err);
    },
    updateFoodRecord: function() {
        var total = $("#totalcal").val();
        var year = $("#year").val();
        var month = $("#month").val();
        var day = $("#day").val();
        var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
        db.transaction(function(tx) {            
            tx.executeSql('UPDATE HISTORYRECORD SET calorie="'+total+'" WHERE year="'+year+'" AND month="'+month+'" AND day="'+day+'"');
        }, app.errorCB, app.updateSuccess);    
    },
    updateSuccess: function() {
        localStorage.clear();

        var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
        db.transaction(function(tx) {            
            tx.executeSql('DELETE FROM TEMPFOODRECORD');
        }, app.errorCB);

        $('.layer-overlay2').addClass('active');
        
        setTimeout(function()
        {
            window.location.href = "record_history.html";
        }, 400);
    },
    backToFoodRecord: function() {
        $('.layer-overlay2').addClass('active');
        
        setTimeout(function()
        {
            window.location.href = "record_food.html";
        }, 400);
    }
};
