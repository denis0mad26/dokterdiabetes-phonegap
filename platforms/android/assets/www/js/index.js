/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() {   
        app.receivedEvent('deviceready');

        var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
        db.transaction(function(tx) {
            tx.executeSql('CREATE TABLE IF NOT EXISTS CREDENTIAL(id INTEGER PRIMARY KEY, nama, hp, kota, photo, token, eventnot, chatnot, replynot, reminder)');
            tx.executeSql('CREATE TABLE IF NOT EXISTS HISTORYRECORD(id INTEGER PRIMARY KEY, glucose, weight, calorie, year, month, day)');
            tx.executeSql('CREATE TABLE IF NOT EXISTS CHATSCHEDULE(id INTEGER PRIMARY KEY, doctor, day, starttime, endtime)');
            tx.executeSql('CREATE TABLE IF NOT EXISTS MITOSFAKTA(id INTEGER PRIMARY KEY, title, desc, mitfak, ispublish, publishdate, createdate, image)');
            tx.executeSql('CREATE TABLE IF NOT EXISTS CALORIE(id INTEGER PRIMARY KEY, foodname, weight, calorie)');
            tx.executeSql('CREATE TABLE IF NOT EXISTS CALORIEIDEAL(id INTEGER PRIMARY KEY, gender, aktivitas, berat, tinggi)');
            tx.executeSql('CREATE TABLE IF NOT EXISTS TEMPMESSAGE(id INTEGER PRIMARY KEY, message, date)');
            tx.executeSql('CREATE TABLE IF NOT EXISTS DOCTOR(id INTEGER PRIMARY KEY, nama, spesialis, photo, hospital)');
            tx.executeSql('CREATE TABLE IF NOT EXISTS TEMPFOODRECORD(id INTEGER PRIMARY KEY, type, foodname, qty, tcal)');
        }, app.errorCB, app.checkUser);        
    },
    errorPop: function(tx, err) {
        alert("Error populating DB: "+err);
    },
    errorCB: function(tx, err) {
        alert("Error processing SQL: "+err);
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var pushNotification = window.plugins.pushNotification;
        pushNotification.register(app.successHandler, app.errorHandler,{"senderID":"141232640260","ecb":"app.onNotificationGCM"});
        //window.plugins.appsFlyer.notifyAppID("com.dokterdiabetes", "CeoG8aSCS3T6m5dajPhFuJ");
        //Flurry.startSession("CHP8XSNZQ58VS3NGJ3GM");
    },
    successHandler: function(result) {
        /*alert('Callback Success! Result = '+result)*/
    },
    errorHandler:function(error) {
        alert(error);
    },
    onNotificationGCM: function(e) {
        switch( e.event )
        {
            case 'registered':
                if ( e.regid.length > 0 )
                {
                    //console.log("Regid " + e.regid);
                    //alert('registration id = '+e.regid);
                    //alert(device.uuid);
                    $.ajax({
                        type: "POST",
                        url: host+"api/sendRegId",
                        data: { token:device.uuid,regid:e.regid },
                        dataType: "json"
                    }).done(function(data) {    
                    }).fail(function( jqXHR, textStatus ) {
                        $('#modal-loading-failed').show();
                        return;
                    });
                }
            break;
 
            case 'message':
              // this is the actual push notification. its format depends on the data model from the push server
              //alert('message = '+e.message+' msgcnt = '+e.msgcnt);
                if(e.msgcnt=="1") {
                    window.location.href = "article.html";
                }
                if(e.msgcnt=="2") {
                    window.location.href = "message.html";
                }
                if(e.msgcnt=="3") {
                    window.location.href = "chat.html";
                }
                if(e.msgcnt=="4") {
                    window.location.href = "mof.html";
                }
            break;
 
            case 'error':
              alert('GCM error = '+e.msg);
            break;
 
            default:
              alert('An unknown GCM event has occurred');
              break;
        }
    },
    checkUser: function() {
        var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
        db.transaction(app.checkQuery);
    },
    checkQuery: function(tx) {
        tx.executeSql('SELECT * FROM CREDENTIAL', [], app.querySuccess);
    },
    querySuccess: function(tx, results) {
        if (results.rows.length>0) {
            $("#token").val(device.uuid);
            sflag = 0;
        } else {
            app.checkServerUser(device.uuid);
            //$("#token").val(device.uuid);
            //sflag = 1;
        }
        $('#modal-loader').show();
        app.getMitfak();
        app.getLastCalorie();
        app.getChatSchedule();
        app.getLastDoctor();
        app.checkHistoryData();
        $('#modal-loader').hide();
    },
    checkServerUser: function(uuid) {
        $.ajax({
            type: "GET",
            url: host+"api/checkServerUser",
            data: { token:uuid },
            dataType: "json"
        }).done(function(data) {            
            var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
            db.transaction(function(tx) {
                tx.executeSql('INSERT INTO CREDENTIAL(id,nama,hp,kota,photo,token,eventnot,chatnot,replynot,reminder) VALUES (null,"'+data.nama+'","'+data.hp+'","'+data.kota+'","'+data.photo+'","'+data.token+'", "true", "true", "true", "true")');
            },app.errorCB);
            $("#token").val(device.uuid);
            sflag = 0;           
        }).fail(function(jqXHR, textStatus) {
            $("#token").val(device.uuid);
            sflag = 1;
            //$('#modal-loading-failed').show();
            return;
        });
    },
    /*dropMitosfakta: function() {
        var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
        db.transaction(function(tx) {
            tx.executeSql('SELECT * FROM MITOSFAKTA ORDER BY id DESC LIMIT 1', [], function(tx,results) {
                var mflen = results.rows.length;
                var mfid;
                if (mflen>0) {
                    mfid=results.rows.item(0).id;
                } else {
                    mfid=0;
                }
                app.getMitfak(mfid);
            }, app.errorCB);
        });
    },*/
    getMitfak: function(lastmfid) {
        $.ajax({
            type: "GET",
            url: host+"api/getMitosFakta",
            //data: { lastmfid:lastmfid },
            dataType: "json"
        }).done(function(data) {  
            var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
            db.transaction(function(tx) {
                tx.executeSql('DELETE FROM MITOSFAKTA');
                $.each(data, function(i,val) {
                    var pdate = new Date(val.publishSchedule);
                    var pday = pdate.getDate();
                    var pmonth = pdate.getMonth()+1;
                    var pyear = pdate.getFullYear(); 
                    var pfdate = pday+"/"+pmonth+"/"+pyear;
                    var cdate = new Date(val.publishSchedule);
                    var cday = cdate.getDate();
                    var cmonth = cdate.getMonth()+1;
                    var cyear = cdate.getFullYear(); 
                    var cfdate = cday+"/"+cmonth+"/"+cfdate;
                    tx.executeSql('INSERT INTO MITOSFAKTA(id,title, desc, mitfak, ispublish, publishdate, createdate, image) VALUES("'+val.id+'","'+val.mfTitle+'","'+val.mfDesc+'","'+val.mfMitfak+'","'+val.isSchedule+'","'+pfdate+'","'+cfdate+'","'+val.mfImage+'")');
                }); 
            });                
        }).fail(function(jqXHR, textStatus) {
            $('#modal-loading-failed').show();
            return;
        });
    },
    getLastCalorie: function() {
        var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
        db.transaction(function(tx) {
            tx.executeSql('SELECT * FROM CALORIE ORDER BY id DESC LIMIT 1', [], function(tx,results) {
                var clen = results.rows.length;
                var cid;
                if (clen>0) {
                    cid=results.rows.item(0).id;
                } else {
                    cid=0;
                }
                app.getCalorie(cid);
            }, app.errorCB);
        });
    },
    getCalorie: function(lastcal) {
        $.ajax({
            type: "GET",
            url: host+"api/getCalorie",
            data: { lastcal:lastcal },
            dataType: "json"
        }).done(function(data) {  
            var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
            db.transaction(function(tx) {
                $.each(data, function(i,val) {
                    tx.executeSql('INSERT INTO CALORIE(id, foodname, weight, calorie) VALUES("'+val.id+'","'+val.foodname+'","'+val.weight+'","'+val.calorie+'")');
                }); 
            });                
        }).fail(function(jqXHR, textStatus) {
            $('#modal-loading-failed').show();
            return;
        });
    },
    getChatSchedule: function() {
        $.ajax({
            type: "GET",
            url: host+"api/getChatSchedule",
            dataType: "json"
        }).done(function(data) {  
            var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
            db.transaction(function(tx) {
                tx.executeSql('DELETE FROM CHATSCHEDULE');
                $.each(data, function(i,val) {                   
                    tx.executeSql('INSERT INTO CHATSCHEDULE(id, doctor, day, starttime, endtime) VALUES("'+val.id+'","'+val.dokter.id+'","'+val.day+'","'+val.startTime+'","'+val.endTime+'")');
                });
            }, app.errorCB);                
        }).fail(function(jqXHR, textStatus) {
            $('#modal-loading-failed').show();
            return;
        });
    },
    userInserted: function() {
        app.receivedEvent('deviceready');
        sflag = 0;
        loginCheck(sflag, '#modal-signup', 'consult.html');
        $('.modal').hide();
    },
    getLastDoctor: function() {
        var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
        db.transaction(function(tx) {
            tx.executeSql('SELECT * FROM DOCTOR ORDER BY id DESC LIMIT 1', [], function(tx,results) {
                var dlen = results.rows.length;
                var docid;
                if (dlen>0) {
                    docid=results.rows.item(0).id;
                } else {
                    docid=0;
                }
                app.getDoctor(docid);
            }, app.errorCB);
        });
    },
    getDoctor: function() {
        $.ajax({
            type: "GET",
            url: host+"api/getDoctor",
            dataType: "json"
        }).done(function(data) {  
            var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
            db.transaction(function(tx) {
                $.each(data, function(i,val) {
                    tx.executeSql('INSERT INTO DOCTOR(id, nama, spesialis, photo, hospital) VALUES("'+val.id+'","'+val.fullname+'","'+val.specialist+'","'+val.photo+'","'+val.hospital+'")');
                }); 
            });                
        }).fail(function(jqXHR, textStatus) {
            $('#modal-loading-failed').show();
            return;
        });
    },
    checkHistoryData: function() {        
        var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
        db.transaction(function(tx) {
            for(var i=59;i>=0;i--) {
                var date = new Date();
                var newdate = new Date(date);
                newdate.setDate(newdate.getDate() - i);
                var nd = new Date(newdate);
                var yr = nd.getFullYear();
                var mt = nd.getMonth()+1;
                var dy = nd.getDate();
                tx.executeSql('INSERT INTO HISTORYRECORD SELECT null,0,0,0,"'+yr+'","'+mt+'","'+dy+'" WHERE NOT EXISTS (SELECT 1 FROM  HISTORYRECORD WHERE year="'+yr+'" AND month="'+mt+'" AND day="'+dy+'")'); 
            }
        }, app.errorCB);       
    }
};
