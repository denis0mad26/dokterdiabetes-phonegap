var page=0;

var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() { 
        app.getMythfact();
    },
    errorPop: function(tx, err) {
        alert("Error populating DB: "+err);
    },
    errorCB: function(tx, err) {
        alert("Error processing SQL: "+err);
    },
    getMythfact: function() {     
        var date = new Date();
        var day = date.getDate();
        var month = date.getMonth()+1;
        var year = date.getFullYear(); 
        var pdate = day+"/"+month+"/"+year;
        var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
        db.transaction(function(tx) {       
            tx.executeSql('SELECT * FROM MITOSFAKTA WHERE publishdate="'+pdate+'"', [], function(tx,results) {
                var mflen = results.rows.length;
                if (mflen>0) {
                    $(".mof-question-text").text(results.rows.item(page).title);
                    $(".mof-answer-text").text(results.rows.item(page).desc);
                    if(results.rows.item(page).mitfak=='mitos') {
                        $('.mof-button.fakta').removeClass("rightAnswer");
                        $('.mof-button.mitos').addClass("rightAnswer");
                    }
                    if(results.rows.item(page).mitfak=='fakta') {
                        $('.mof-button.mitos').removeClass("rightAnswer");
                        $('.mof-button.fakta').addClass("rightAnswer");
                    }
                    $(".mof-img").remove();
                    var htm='<img class="mof-img" src="http://articles.appdokter.com/'+results.rows.item(page).image+'" alt=" "/>';
                    $(htm).insertBefore($(".mof-body"));
                    if(page==0) {
                        $(".prev").removeAttr("onclick");
                    } else {
                        $(".prev").attr("onclick","app.prev()");
                    }
                    if(page==(mflen-1)) {
                        $(".next").removeAttr("onclick");
                    } else {
                        $(".next").attr("onclick","app.next()");
                    }
                } else {
                    alert("belum ada mitos dan fakta");
                }
            }, app.errorCB);
        });
    },
    prev: function() {
        page = page-1;
        $('.mof-guide').show();
        $('.mof-answer-cont').hide();
        $('.mof-answer-result-text').hide();
        $('.mof-share-cont').hide();    
        $('.mof-button').removeClass('active');

        $('.mof-button.mitos').fastClick(function()
        {
            if($(this).hasClass('rightAnswer'))
            {
                mofShowResult('text1');
            }
            else
            {
                mofShowResult('text2');
            }
            
            $(this).addClass('active');
        });
        
        $('.mof-button.fakta').fastClick(function()
        {
            if($(this).hasClass('rightAnswer'))
            {
                mofShowResult('text3');
            }
            else
            {
                mofShowResult('text4');
            }
            
            $(this).addClass('active');
        });

        app.getMythfact();
    },
    next: function() {
        page = page+1;
        $('.mof-guide').show();
        $('.mof-answer-cont').hide();
        $('.mof-answer-result-text').hide();
        $('.mof-share-cont').hide();        
        $('.mof-button').on();

        $('.mof-button').removeClass('active');

        $('.mof-button.mitos').fastClick(function()
        {
            if($(this).hasClass('rightAnswer'))
            {
                mofShowResult('text1');
            }
            else
            {
                mofShowResult('text2');
            }
            
            $(this).addClass('active');
        });
        
        $('.mof-button.fakta').fastClick(function()
        {
            if($(this).hasClass('rightAnswer'))
            {
                mofShowResult('text3');
            }
            else
            {
                mofShowResult('text4');
            }
            
            $(this).addClass('active');
        });
        
        app.getMythfact();
    }
};
