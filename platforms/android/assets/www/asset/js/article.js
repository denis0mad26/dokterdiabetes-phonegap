var isLoading = 0;
var viewportH;
var scrollH;
var scrollT;

function calculateHeight()
{
	viewportH = $('.article-content-subcont').height();
	scrollH = $('.article-content-subcont')[0].scrollHeight;
	scrollT = $('.article-content-subcont').scrollTop();
}

// LOADING ARTICLE (EDIT DI SINI)
function articleLoad()
{
	$('.article-list-loading').show();
	isLoading = 1;
	
	/*
		AJAX Load di sini
	*/
}


// WINDOW RESIZE //
$(window).resize(function()
{
	calculateHeight();
});


// MAIN //
$(document).ready(function()
{
	calculateHeight();
	
	
	if(viewportH >= scrollH)
	{
		$('.article-content-subcont').fastClick(function()
		{
			if(isLoading == 0)
			{
				articleLoad();
			}
		});
	}
	else
	{
		$('.article-content-subcont').scroll(function()
		{
			scrollT = $('.article-content-subcont').scrollTop();
			
			if(scrollT + viewportH >= scrollH && isLoading == 0)
			{
				articleLoad();
			}
		});
	}
});